package vending.model

import vending.model.DataTypes.Good
import vending.model.Exceptions.{InsufficientBalanceException, OutOfStockException, ItemNotFound, UnsupportedCoinException}
import scala.collection.concurrent.TrieMap

object DataTypes {

  case class Good(title: String, price: Long, qty: Int)

}


object Storage {

  val goods = TrieMap(1 -> Good("Nuts", 95, 1), 2 -> Good("Pepsi", 60, 8), 3 -> Good("Pringles", 200, 10))

  // cents -> qty of coins in the machine
  val coins = TrieMap(1 -> 10, 5 -> 20, 10 -> 20, 25 -> 40, 50 -> 100, 100 -> 100)

  var balance = 0L

}

object RPC {

  val values = Storage.coins.keySet.seq

  def getBalance = Map("balance" -> Storage.balance)

  def getGoods = Storage.goods.seq

  def buyGood(idx: Int) = {
    val indexes = Storage.goods.keySet.seq
    if (!indexes.contains(idx)) throw new ItemNotFound(idx)
    val good = Storage.goods(idx)
    if (good.qty == 0) throw new OutOfStockException(idx)
    if (good.price > Storage.balance) throw new InsufficientBalanceException(Storage.balance, good.price)
    Storage.balance = Storage.balance - good.price
    Storage.goods.update(idx, good.copy(qty = good.qty - 1))
    Map("message" -> s"Here is your good: ${good.title}! Remains: ${good.qty - 1}")
  }

  def insertCoin(value: Int) = {
    if (!values.contains(value)) throw new UnsupportedCoinException(value)
    Storage.balance += value
    Storage.coins.update(value, Storage.coins(value) + 1)
    Map("message" -> "ok")
  }

}