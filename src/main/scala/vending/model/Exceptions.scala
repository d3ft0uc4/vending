package vending.model


object Exceptions {

  case class InsufficientBalanceException(balance: Long, price: Long) extends Exception()

  case class ItemNotFound(idx: Int) extends Exception()

  case class OutOfStockException(idx: Int) extends Exception()

  case class UnsupportedCoinException(value: Int) extends Exception()

}
