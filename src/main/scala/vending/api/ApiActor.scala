package vending.api

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRefFactory
import akka.util.Timeout
import spray.httpx.Json4sJacksonSupport
import spray.routing.{ExceptionHandler, HttpService}
import org.json4s.{DefaultFormats, Formats}
import scala.concurrent.duration._
import vending.model.Exceptions.{UnsupportedCoinException, OutOfStockException, ItemNotFound, InsufficientBalanceException}
import vending.model.RPC


class ApiActor extends WebService {

  def actorRefFactory: ActorRefFactory = context

}

trait WebService extends HttpService with Json4sJacksonSupport with Actor with ActorLogging {

  override implicit def json4sJacksonFormats: Formats = DefaultFormats

  implicit val eh = myExceptionHandler
  implicit val timeout = new Timeout(5 seconds)

  def receive = runRoute(route)

  val route = getGoods ~ getBalance ~ insertCoin ~ buyGood

  def getGoods = get {
    path("good") {
      complete(RPC.getGoods)
    }
  }

  def buyGood = post {
    path("good") {
      parameter('idx.as[Int]) {
        idx => complete(RPC.buyGood(idx))
      }
    }
  }

  def getBalance = get {
    path("money") {
      complete(RPC.getBalance)
    }
  }

  def insertCoin = post {
    path("money") {
      parameter('value.as[Int]) {
        value => complete(RPC.insertCoin(value))
      }
    }
  }


  private def myExceptionHandler: ExceptionHandler = {
    ExceptionHandler.apply {
      case ItemNotFound(idx) =>
        complete(404, Map("message" -> s"Item with idx $idx not found."))
      case InsufficientBalanceException(balance, price) =>
        complete(400, Map("message" -> s"Not enough balance to perform op. Required: $price cents, actual: $balance cents"))
      case OutOfStockException(idx) =>
        complete(400, Map("message" -> s"Item with index: $idx is out of stock."))
      case UnsupportedCoinException(value) =>
        complete(400, Map("message" -> s"Coin with value $value is not supported."))
      case e: Exception =>
        requestUri { uri =>
          log.error(e, s"Request to $uri could not be handled normally")
          complete(e.getMessage)
        }
    }
  }

}
