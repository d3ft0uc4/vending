package vending

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import spray.can.Http
import scala.concurrent.duration._
import vending.api.ApiActor


object Boot extends App {
  implicit val system = ActorSystem()
  implicit val timeout = new Timeout(5 seconds)
  val serviceApi = system.actorOf(Props[ApiActor], "MainApiActor")
  IO(Http) ? Http.Bind(serviceApi, interface = "localhost",port = 35790)
}
