import sbt.Keys._
import sbt._
import sbtassembly.{AssemblyKeys, MergeStrategy}
import AssemblyKeys._

object Resolvers {
  val sprayRepo = "spray repo" at "http://repo.spray.io"
  val typeSafeRepo = Classpaths.typesafeReleases

  def commonResolvers = Seq(sprayRepo, typeSafeRepo)
}

object Dependencies {

  val json4sJackson = "org.json4s" %% "json4s-jackson" % "3.5.0"
  val json4sExt = "org.json4s" %% "json4s-ext" % "3.5.0"

  val sprayCan = "io.spray" %% "spray-can" % "1.3.4"
  val sprayRouting = "io.spray" %% "spray-routing" % "1.3.4"
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % "2.3.15"
}


object BuildSettings {

  val Organization = "com.test"
  val Name = "vending"
  val Version = "0.0.1-SNAPSHOT"
  val ScalaVersion = "2.11.8"
  val IsSnapshot = Version.trim.endsWith("SNAPSHOT")

  val buildSettings = Seq(
    organization := Organization,
    name := Name,
    version := Version,
    scalaVersion := ScalaVersion
  )
}

object MainBuild extends Build {

  import BuildSettings._
  import Dependencies._
  import Resolvers._

  val deps = Seq(
    json4sJackson,
    json4sExt,sprayCan,
    sprayRouting,akkaActor
  )

  javacOptions ++= Seq("-encoding", "UTF-8")

  lazy val main = Project(
    Name,
    file("."),
    settings = buildSettings ++ Defaults.coreDefaultSettings ++
      Seq(
        libraryDependencies ++= deps,
        resolvers := commonResolvers
      ))

  assemblyMergeStrategy in assembly := {
    case x => MergeStrategy.first
  }

}

