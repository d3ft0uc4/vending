# VENDING MACHINE #

This is a simple implementation of the vending machine.


## SETUP ##

* Clone the repository
* cd to the project folder
* type 'sbt run' to start
* Now server listening 35790 port on the localhost

## USAGE ##

### Get the goods: ###
* GET http://localhost:35790/good
* Returns: JSON: goodID - description


### Buy the good: ###

* POST http://localhost:35790/good?idx=idx
* Param: idx: index of the good (1,2,3 in this example)

### Get the balance: ###

* GET http://localhost:35790/money
* Returns: JSON: {"balance":balance}

### Insert a coin: ###
* POST http://localhost:35790/money?value=value
* Param: value: value of the coin in cents (Only 1,5,10,25,50,100 are supported)